public class Main {
    public static void main(String[] args) {
        people people = new peopleBuilder()
                .name("juan")
                .age(18)
                .lastName("Perez")
                .telephone(71784041)
                .nationality("Bolivia")
                .build();
    }
}
