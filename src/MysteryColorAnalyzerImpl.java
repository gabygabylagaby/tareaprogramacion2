import java.util.ArrayList;
import java.util.List;


public class MysteryColorAnalyzerImpl implements MysteryColorAnalyzer{

    @Override
    public int numberOfDistinctColors(List<Color> mysteryColors) {
        int countNumberColors=0;
        ArrayList<Color> distinctColors = new ArrayList<Color>();
        for (Color colors : mysteryColors) {
            countNumberColors ++;
            if (!distinctColors.equals(colors)){
                distinctColors.add(colors);
                countNumberColors ++;
            }
        }
        return countNumberColors;
    }

    @Override
    public int colorOccurrence(List<Color> mysteryColors, Color color) {
        int countColor = 0;
        for (Color mysteryColorsAnalyzer : mysteryColors){
            if (mysteryColorsAnalyzer == color){
                countColor ++;
            }
        }
        return countColor;
    }
}
