/**
 * The people class will have the goal of creating people without the design pattern.
 */
public class people {
    private int age,telephone;
    private String name;
    private String lastName;
    private String nationality;
    private final static String GREET = "hello";

    private final String greet() {
        return GREET + " my name is " + name  ;
    }

    /**
     * The people builder has the objective of creating through the Builder design pattern
     *
     * In case we do not have the name, the due request will be made.
     * @param builder is a reference to the design pattern.
     */
    public people(peopleBuilder builder){
        if(builder.getName() == null){
            throw new IllegalArgumentException("We require the name.");
        }
        this.name = builder.getName();
        this.lastName = builder.getLastName();
        this.nationality = builder.getNationality();
        this.age = builder.getAge();
        this.telephone = builder.getTelephone();
    }

}
