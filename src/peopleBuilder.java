/**
 *This class is solely for building a person via the Builder design pattern.
 */
public class peopleBuilder {
    private int age,telephone;
    private String name, lastName, nationality;

    /**
     *The due instance in the person's name may change with different people.
     * @param name of the person
     * @return the design pattern or also said in another way the name of the person.
     */
    public peopleBuilder name (String name){
        this.name = name;
        return this;
    }

    /**
     *The due instance in the person's last Name may change with different people.
     *
     * @param lastName the person's last name
     * @return the design pattern or also said in another way the last name of the person.
     */
    public peopleBuilder lastName (String lastName){
        this.lastName = lastName;
        return this;
    }

    /**
     * The due instance in the person's nationality may change with different people.
     *
     * @param nationality the person's nationality
     * @return the design pattern or also said in another way the nationality of the person.
     */
    public peopleBuilder nationality (String nationality){
        this.nationality = nationality;
        return this;
    }

    /**
     * The due instance in the person's age may change with different people.
     *
     * @param age the person's age
     * @return the design pattern or also said in another way the age of the person.
     */
    public peopleBuilder age (int age){
        this.age = age;
        return this;
    }

    /**
     * The due instance in the person's telephone may change with different people.
     *
     * @param telephone the person's age
     * @return the design pattern or also said in another way the telephone of the person.
     */

    public peopleBuilder telephone (int telephone){
        this.telephone = telephone;
        return this;
    }

    /**
     *The builder method is the creation of another person.
     * @return a new instance.
     */
    public people build(){
        return new people(this);
    }

    /**
     * This method returns the age of the person.
     * @return people's age.
     */
    public int getAge() {
        return age;
    }

    /**
     * This method returns the Telephone of the person.
     *
     * @return people's Telephone.
     */
    public int getTelephone() {
        return telephone;
    }

    /**
     * This method returns the name of the person.
     *
     * @return people's name.
     */

    public String getName() {
        return name;
    }

    /**
     * This method returns the last Name of the person.
     *
     * @return people's last Name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * This method returns the nationality of the person.
     *
     * @return people's nationality.
     */
    public String getNationality() {
        return nationality;
    }
}

