# TareaProgramacion2

## Task Description

During the day of July 25, 2022, the trainer, seeing the aptitudes of 
the students for the first task, asked us for two tasks, 
one of them is to be able to successfully achieve the encapsulation, 
taking into account that the students already have the necessary and 
effective knowledge to accomplish this task, gave us until 8:00 p.m. to 
complete this task.
As a challenge, I decided to learn a bit about the Builder design pattern, so I made it for this task, for educational purposes only.

## Add your files

- [ ] [Encapsulate properly the class by providing read accessors (setters are not required for this Kata).
  Implement the Builder design Pattern.
  Additionally we have allegedly a JIT compiler and we want to do some micro-optimization by marking constants in the code.]
- [ ] [Implement the Builder design Pattern.]


- [ ] [Create the implementation for the interface below. The implementation needs to be called "MysteryColorAnalyzerImpl".]
- 
```

import java.util.List;

public interface MysteryColorAnalyzer {
    /**
     * This method will determine how many distinct colors are in the given list
     *
     * <p>
     * Colors are defined via the {@link Color} enum.
     * </p>
     *
     * @param mysteryColors list of colors from which to determine the number of distinct colors
     * @return number of distinct colors
     */
    int numberOfDistinctColors(List<Color> mysteryColors);

    /**
     * This method will count how often a specific color is in the given list
     *
     * <p>
     * Colors are defined via the {@link Color} enum.
     * </p>
     *
     * @param mysteryColors list of colors from which to determine the count of a specific color
     * @param color color to count
     * @return number of occurrence of the color in the list
     */
    int colorOccurrence(List<Color> mysteryColors, Color color);
}
```
Color has the following enum structure, but hey you really don't need to know all these colors if you're implementing the methods properly.
```
public enum Color {
    RED, GREEN, BLUE
}

public class People{
   public int age;
   public String name;
   public String lastName;
   String GREET="hello";
   
   public String greet(){
     return GREET+" my name is "+name;
   }
 }
 

```
